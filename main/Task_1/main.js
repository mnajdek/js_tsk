//Initialize the repo // 

var personFactory  = function () {
    var details = {
        firstName: 'John',
        lastName: 'Example',
        accounts: [
            privateAccount = {
                balance: 0.5,
                currency: 'BTC'
            }
        ]
        }
        return {
            
            newFirstName: details.firstName,
            newLastName: details.lastName,

            sayHello: function (){
                return 'Hello ' + this.newFirstName +  ' ' + this.newLastName + ' ' + details.accounts.length;
            } 
        }
    };

newPerson = new personFactory();
console.log(newPerson.sayHello());

