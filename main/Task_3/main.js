//Initialize the repo // 

var Account = function(balance, currency){
    this.balance = balance;
    this.currency = currency;
}

var person  = (function (){
    var details = {
        firstName: 'John',
        lastName: 'Example'
            };
    
        calculateBalance = function() {
            var accSum = 0;            
            for (var i=0; i < this.accounts.length; i++){
                accSum += this.accounts[i].balance;
                }
            return accSum;
            };
        return {

            newFirstName: details.firstName,
            newLastName: details.lastName,

            accounts: [
                {
                    balance: 1000,
                    currency: 'PLN'
                    },
                {
                    balance: 50000,
                    currency: 'PLN'
                    }
            ],
            addAccount: function(newBalance, newCurrency){
                person.accounts.push({balance:newBalance, currency:newCurrency})
                },       
            sayHello: function (){
                return 'Hello ' + this.newFirstName +  ' ' + this.newLastName + ' ' + calculateBalance.call(this);
                }
            };
})();


person.accounts[person.accounts.length] = new Account(2500,"PLN")
console.log(person.accounts);
console.log(person.sayHello());
console.log("Print results:")
console.log(person.addAccount(5000,'PLN'));
console.log(person.sayHello());
console.log(person.accounts);