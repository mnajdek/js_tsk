//Initialize the repo // 

class Account {
    constructor(balance, currency) {
        this.balance = balance;
        this.currency = currency;
    }
 }
 class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };
    _calculateBalance() {
        var accSum = 0;
        for (let account of this.accounts) {
            accSum += account.balance;
        }
        return accSum;
    };
   
    sayHello() {
        let accMessage = " ";
        let accLength = this.accounts.length;
        if(accLength == 0) {
            accMessage = 'kont.';
        } else if (accLength == 1) {
            accMessage = 'konto i na nim jest:';           
        } else { //More than 1 account
            accMessage = 'konta i na nich jest:';           
        }
        return `Jestem  ${this.firstName}  ${this.lastName},  mam ${accLength} ${accMessage}  ${this._calculateBalance()}`
    }
    filterPositiveAccounts() {
      return this.accounts.filter(elem => elem.balance > 0);
    }
    addAccount(account) {
        this.accounts.push(account);
    }
 }


 const newPerson = new Person("John", "Example", [new Account(4000, "EURO"), new Account(2000, "EURO")]);

 console.log(newPerson.sayHello());
 newPerson.addAccount(new Account(0, "?"));
 console.log(newPerson.filterPositiveAccounts());
 console.log(newPerson.sayHello());

