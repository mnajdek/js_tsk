class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
 }
 class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };
    _calculateBalance() {
        var accSum = 0;
        for (let account of this.accounts) {
            accSum += account.balance;
        }
        return accSum;
    };
    withdraw(accountNumber, amount){
        let personAccount = this.findAccount(accountNumber);
        return new Promise((resolve, reject) => {
            if(personAccount && amount <= personAccount.balance) {
                personAccount.balance -= amount;
                setTimeout( () => {
                    resolve(`Konto ${accountNumber} nowy stan: ${personAccount.balance}, po wyplacie ${amount}`)},3000);
            } else {
                reject('Error contact your bank');
            }
        }
        )
    }

    findAccount = function (accountNumber) {
        return this.accounts.find(account => {
            return account.number === accountNumber});
    }    
 
    sayHello() {
        let accMessage = " ";
        let accLength = this.accounts.length;
        if(accLength == 0) {
            accMessage = 'kont.';
        } else if (accLength == 1) {
            accMessage = 'konto i na nim jest:';           
        } else { //More than 1 account
            accMessage = 'konta i na nich jest:';           
        }
        return `Jestem  ${this.firstName}  ${this.lastName},  mam ${accLength} ${accMessage}  ${this._calculateBalance()}`
    }

    filterPositiveAccounts() {
      return this.accounts.filter(konto => {
            return konto.balance > 0
      });
    }
    addAccount(account) {
        this.accounts.push(account);
    }
 }

 var newAccount = new Account(1000,"EURO",4);
 const newPerson = new Person("John", "Example", [new Account(3000, "EURO",1), new Account(5000, "EURO",2), new Account(0, "EURO",3),newAccount]);
 var konto = new Account(1500,"EURO",5);
 newPerson.addAccount(konto);
 
 console.log(newPerson.sayHello());
 console.log(newPerson.findAccount(3));

 console.log(newPerson.accounts);
 newPerson.withdraw(1,1000)
    .then((success) => {
        console.log(success)
        console.log(newPerson.sayHello());
    }
    ).catch((fail) =>{
        console.log(fail)
    }
    )
   
 newPerson.withdraw(3,5000)
    .then((success) => {
        console.log(success)
        console.log(newPerson.sayHello());        
    }
    ).catch((fail) =>{
        console.log(fail)
        console.log(newPerson.sayHello());        
    }
    )

    