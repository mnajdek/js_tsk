class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
 }
 class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    findAccount(accountNumber){
        return this.accounts.find(account => {
            return account.number === accountNumber});
    }
    _calculateBalance() {
        var accSum = 0;
        for (let account of this.accounts) {
            accSum += account.balance;
        }
        return accSum;
    };

    withdraw(accountNumber, amount){
        let personAccount = this.findAccount(accountNumber);
        return new Promise((resolve, reject) => {
            if(personAccount && amount <= personAccount.balance) {
                personAccount.balance -= amount;
                setTimeout( () => {
                    resolve(`Konto ${accountNumber} nowy stan konta w Euro: ${personAccount.balance}, po wyplacie ${amount}`)},3000);
            } else {
                reject('Error! Contact your bank');
            }
        }
        )
    }
 
    sayHello() {
        let accMessage = " ";
        let accLength = this.accounts.length;
        if(accLength == 0) {
            accMessage = 'kont.';
        } else if (accLength == 1) {
            accMessage = 'konto i na nim jest:';           
        } else { //More than 1 account
            accMessage = 'konta i na nich jest:';           
        }
        return `Jestem  ${this.firstName}  ${this.lastName},  mam ${accLength} ${accMessage}  ${this._calculateBalance()}`
    }

    filterPositiveAccounts() {
      return this.accounts.filter(konto => {
            return konto.balance > 0
      });
    }
    addAccount(account) {
        this.accounts.push(account);
    }
 }

 // const newPerson = new Person("John", "Example", [new Account(3000, "EURO",1), new Account(5000, "EURO",2)]);
