document.addEventListener("DOMContentLoaded", function() {
    
    const submitButton = document.querySelector('.btn.btn-primary');
    
    const accountNumberHTML = document.querySelector('#number');
    const accountAmountHTML = document.querySelector('#amount');
    submitButton.onclick = function() {withdrawalHTML()};
    
    const personFullName = document.querySelector('.card-title');
    function accountDetails () {
    let accountHTML = " ";    
        for (const i in newPerson.accounts) {
            accountHTML += "<p>" + newPerson.accounts[i].balance + " " + newPerson.accounts[i].currency + " " + newPerson.accounts[i].number + "</p>";
            document.querySelector('.card-text').innerHTML = accountHTML;
        }
    };
           personFullName.innerHTML = newPerson.firstName +' '+ newPerson.lastName;
    
           accountDetails();
    
        function withdrawalHTML(){
            let accNumber = parseInt(accountNumberHTML.value);
            let amount = parseInt(accountAmountHTML.value);

            if (amount > 0 && !isNaN(accountAmountHTML.value)) {
                newPerson.withdraw(accNumber,amount)
                .then((success) => {
                    console.log(success)
                    document.querySelector('#info').innerHTML = "Wyplaciles "+ amount + " EURO " + " z konta nr " + accNumber;
                    document.getElementById("info").className = "alert alert-success";
                    accountDetails();
                }
                ).catch((fail) =>{
                    console.log(fail)
                    document.querySelector('#info').innerHTML = "Error! Pieniadze nie wyplacone.";
                    document.getElementById("info").className = "alert alert-danger";                    
                }
                )
            } else if (isNaN(accountAmountHTML.value)) {
                document.querySelector('#info').innerHTML = "Został wprowadzony łańcuch znaków!";
                document.getElementById("info").className = "alert alert-dark";              
            } else {
                    document.querySelector('#info').innerHTML = "Kwota mniejsza lub równa 0!";
                    document.getElementById("info").className = "alert alert-dark";                                   
            }
    
        }
    
    
    
    });