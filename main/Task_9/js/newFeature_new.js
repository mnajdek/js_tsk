
const init = ( () => {

        document.addEventListener("DOMContentLoaded", function() {
            const newPerson = new Person("John", "Example", [new Account(3000, "EURO",1), new Account(5000, "EURO",2)]);
            const submitButton = document.querySelector('.btn.btn-primary');
            const accountNumberHTML = document.querySelector('#number');
            const accountAmountHTML = document.querySelector('#amount');
            submitButton.onclick = function() {withdrawalHTML()}; 
            const personFullName = document.querySelector('.card-title');

            personFullName.innerHTML = newPerson.firstName +' '+ newPerson.lastName;
                        
            function accountDetails () {
            let accountHTML = " ";    
                for (const i in newPerson.accounts) {
                    accountHTML += "<p>" + newPerson.accounts[i].balance + " " + newPerson.accounts[i].currency +
                    " " + newPerson.accounts[i].number + "</p>";
                    document.querySelector('.card-text').innerHTML = accountHTML;
                }
            };

            accountDetails();
            
            function withdrawalHTML(){
                let accNumber = parseInt(accountNumberHTML.value);
                let amount = parseInt(accountAmountHTML.value);

                if (amount > 0 && !isNaN(accountAmountHTML.value)) {
                    newPerson.withdraw(accNumber,amount)
                    .then((success) => {
                        console.log(success)
                        document.querySelector('#info').innerHTML = "Wypłaciłeś "+ amount + " EURO " + " z konta nr " + accNumber + ". <b>Pozostało środków: " + newPerson.accounts[accNumber-1].balance + "</b>";
                        document.getElementById("info").className = "alert alert-success";
                        accountDetails();
                    }
                    ).catch((fail) =>{
                        console.log(fail)
                        document.querySelector('#info').innerHTML = "Error! Pieniądze nie wypłacone.";
                        document.getElementById("info").className = "alert alert-danger";                    
                    }
                    )
                } else if (isNaN(accountAmountHTML.value)) {
                    document.querySelector('#info').innerHTML = "Został wprowadzony łańcuch znaków!";
                    document.getElementById("info").className = "alert alert-dark";              
                } else {
                        document.querySelector('#info').innerHTML = "Kwota mniejsza lub równa 0!";
                        document.getElementById("info").className = "alert alert-dark";                                   
                }    
            }
        });

    function onChanged() {
        document.addEventListener("DOMContentLoaded", function() {
        const withdrawButton = document.querySelector(".btn.btn-primary");
        const idInput = document.querySelector("#number");
        const amountInput = document.querySelector("#amount");
    
        withdrawButton.setAttribute('disabled','disabled');
    
        function inputchecker() {
            let idNumber = idInput.value;
            let amount = amountInput.value;
            if (idNumber && amount) {
                withdrawButton.disabled = false;
            } else {
                withdrawButton.disabled = true;
            }
        }
    
        idInput.addEventListener("change", inputchecker);
        amountInput.addEventListener("change", inputchecker);
        idInput.addEventListener("keyup", inputchecker);
        amountInput.addEventListener("keyup", inputchecker);
    })};
    return {
        onChanged: onChanged()
    }
})();